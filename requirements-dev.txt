pytest
pytest-cov
pytest-mock
flit
black
flake8
mypy
codespell
docutils
mkdocs
mkautodoc
mkdocs-material
# you also need these non-Python packages:
# Debian and derivatives: make shunit2 git ccache gcc perl findutils xz-utils bzip2
# Fedora and derivatives: make shunit2 git ccache gcc perl findutils xz bzip2 perl-JSON-PP
